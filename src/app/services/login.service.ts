import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/user.model';

const { userAPI, apiKey } = environment;

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  // DI
  constructor(private readonly http: HttpClient) {}

  public login(username: string): Observable<User> {
    return this.checkUser(username)
      .pipe(
        switchMap((user: User | undefined) => {
          if (!user) {
            return this.createUser(username);
          }
          return of(user);
        })
      )
  }

  // Check the user - MAP from [] - Single User.
  private checkUser(username: string): Observable<User | undefined> {
    return this.http.get<User[]>(`${userAPI}?username=${username}`).pipe(
      map((users: User[]) => {
        return users.pop();
      })
    );
  }

  // Create the user
  private createUser(username: string): Observable<User> {
    const user = {
      username,
      favourites: []
    };

    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-api-key": apiKey
    });

    return this.http.post<User>(userAPI, user, { headers })
  }
}
