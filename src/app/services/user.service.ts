import { Injectable } from '@angular/core';
import { User } from '../models/user.model';

const USER_STORAGE_KEY = 'user';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  public user: User | undefined;

  constructor() {
    const storedUser = sessionStorage.getItem(USER_STORAGE_KEY);
    if (!storedUser) {
      return;
    }

    try {
      const json = JSON.parse(storedUser);
      this.user = json as User;
    } catch (e) {
      sessionStorage.removeItem(USER_STORAGE_KEY);
    }
  }

  public setUser(user: User): void {
    this.user = user;
    sessionStorage.setItem(USER_STORAGE_KEY, JSON.stringify(user));
  }
}
