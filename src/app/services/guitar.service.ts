import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Guitar } from '../models/guitar.model';

const { guitarAPI } = environment;

@Injectable({
  providedIn: 'root', // Singleton
})
export class GuitarService {
  // Dependency Injection
  constructor(private readonly http: HttpClient) {}

  public findAllGuitars(): Observable<Guitar[]> {
    return this.http.get<Guitar[]>(guitarAPI);
  }
}
