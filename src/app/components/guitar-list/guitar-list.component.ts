import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Guitar } from 'src/app/models/guitar.model';

@Component({
  selector: 'app-guitar-list',
  templateUrl: './guitar-list.component.html',
  styleUrls: ['./guitar-list.component.css']
})
export class GuitarListComponent {

  @Input() guitars: Guitar[] = [];
  @Output() clicked: EventEmitter<Guitar> = new EventEmitter();

  constructor() { }

  public onGuitarClick(guitar: Guitar): void {
    this.clicked.emit(guitar);
  }

}
