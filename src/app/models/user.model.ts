export interface User {
  id: number;
  username: string;
  favourites: string[];
}
