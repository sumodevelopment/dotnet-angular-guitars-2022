export interface Guitar {
  id: string;
  model: string;
  manufacturer: string;
  bodyType: string;
  materials: GuitarMaterials;
  strings: number;
  image: string;
}

interface GuitarMaterials {
  neck: string;
  fretboard: string;
  body: string;
}
