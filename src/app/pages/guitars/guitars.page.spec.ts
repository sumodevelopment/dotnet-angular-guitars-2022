import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GuitarsPage } from './guitars.page';

describe('GuitarsPage', () => {
  let component: GuitarsPage;
  let fixture: ComponentFixture<GuitarsPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GuitarsPage ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GuitarsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
