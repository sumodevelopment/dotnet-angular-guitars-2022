import { Component, OnInit } from '@angular/core';
import { Guitar } from 'src/app/models/guitar.model';
import { GuitarService } from 'src/app/services/guitar.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-guitars',
  templateUrl: './guitars.page.html',
  styleUrls: ['./guitars.page.css'],
})
export class GuitarsPage implements OnInit {
  public guitars: Guitar[] = [];
  public message: string = '';

  // DI
  constructor(
    private readonly userService: UserService,
    private readonly guitarService: GuitarService
    ) {}

  public ngOnInit(): void {
    this.guitarService.findAllGuitars().subscribe({
      next: (guitars: Guitar[]) => {
        this.guitars = guitars;
      },
      error: () => {},
      complete: () => {},
    });
  }

  public handleGuitarClick(guitar: Guitar) {
    this.message = `You just clicked on the ${guitar.model} guitar! 💩`;
  }
}
