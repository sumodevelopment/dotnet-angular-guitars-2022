import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css'],
})
export class LoginPage {
  // DI
  constructor(
    private readonly router: Router,
    private readonly loginService: LoginService,
    private readonly userService: UserService) {}

  public onSubmit(form: NgForm): void {
    console.log(form.value); // all inputs linked to the form
    const { username } = form.value;
    if (username.trim() === '') {
      throw new Error("That's not a username, Hackerman!");
    }

    this.loginService.login(username).subscribe({
      next: (user: User) => {
        console.log(user);
        // Save the user
        this.userService.setUser(user);
        // redirect to the new page (Guitars)
        this.router.navigateByUrl("/guitars");
        // Some check, when redirect.
      },
    });
  }
}
