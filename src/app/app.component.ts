import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root', // How we render the component
  templateUrl: './app.component.html', // External file
})
export class AppComponent implements OnInit {
  // Properties - Default to public
  public title = 'ng-guitars';
  public showTitle: boolean = true;
  private hiddenTitle = 'Ng Secret Guitars'; // NOT Accessible in Template
  public guitars: string[] = ['Telecaster', 'Stratocaster', 'Les Paul'];

  constructor() {
    console.log('AppComponent.constructor');
  }

  public ngOnInit(): void {
    setTimeout(() => {
      this.title = "Ng Guitars 2022";
      this.guitars.push('Rickenbacker');
    }, 2000);
  }

  public onToggleTitleClick(): void {
    this.showTitle = !this.showTitle;
  }
}
